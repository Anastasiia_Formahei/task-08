package com.epam.rd.java.basic.task8.utils;

import java.util.ArrayList;
import java.util.List;

public class FlowerList {

    private ArrayList<Flower> flowers = new ArrayList<>();

    public List<Flower> getFlowers(){
        if(flowers == null){
            flowers = new ArrayList<Flower>();
        }
        return this.flowers;
    }
}
