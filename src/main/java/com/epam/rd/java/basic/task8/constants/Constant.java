package com.epam.rd.java.basic.task8.constants;

public class Constant {
    public static final String FLOWER = "flower";
    public static final String NAME = "name";
    public static final String SOIL = "soil";
    public static final String ORIGIN = "origin";
    public static final String VP = "visualParameters";
    public static final String STEMCOLOUR = "stemColour";
    public static final String LEAFCOLOUR = "leafColour";
    public static final String AVELENFLOWER = "aveLenFlower";

    public static final String GT = "growingTips";
    public static final String MULTI = "multiplying";
    public static final String TEMPRETURE = "tempreture";
    public static final String LIGHT = "lighting";
    public static final String WATER = "watering";


}
