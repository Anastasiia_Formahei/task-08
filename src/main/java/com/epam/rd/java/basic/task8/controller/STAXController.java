package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Constant;
import com.epam.rd.java.basic.task8.utils.Flower;
import com.epam.rd.java.basic.task8.utils.FlowerList;
import com.epam.rd.java.basic.task8.utils.GrowingTips;
import com.epam.rd.java.basic.task8.utils.VisualParameters;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private FlowerList flowerList;

	public FlowerList getFlowerList(){
		return flowerList;
	}

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void parse() throws XMLStreamException {

		Flower flower = null;
		String currentElement = null;

		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

		XMLEventReader reader = factory.createXMLEventReader(new StreamSource(xmlFileName));

		while (reader.hasNext()){
			XMLEvent event = reader.nextEvent();
			if(event.isCharacters() && event.asCharacters().isWhiteSpace()){
				continue;
			}
			if(event.isStartElement()){
				StartElement startElement = event.asStartElement();
				currentElement = startElement.getName().getLocalPart();

				if("flowers".equals(currentElement)){
					flowerList = new FlowerList();
					continue;
				}
				if ((Constant.FLOWER.equals(currentElement))){
					flower = new Flower();
					continue;
				}
			}
			if (event.isCharacters()){
				Characters characters = event.asCharacters();
				VisualParameters vp = new VisualParameters();
				GrowingTips gt = new GrowingTips();

				if(Constant.NAME.equals(currentElement)){
					flower.setName(characters.getData());
				}
				if(Constant.SOIL.equals(currentElement)){
					flower.setSoil(characters.getData());
				}
				if(Constant.ORIGIN.equals(currentElement)){
					flower.setOrigin(characters.getData());
				}
				if(Constant.STEMCOLOUR.equals(currentElement)){
					vp.setStemColour(characters.getData());
					flower.setVisualParameters(vp);
				}
				if(Constant.LEAFCOLOUR.equals(currentElement)){
					flower.getVisualParameters().setLeafColour(characters.getData());
				}
				if(Constant.AVELENFLOWER.equals(currentElement)){
					flower.getVisualParameters().setAveLenFlower(characters.getData());
				}
				if(Constant.TEMPRETURE.equals(currentElement)){
					gt.setTempreture(characters.getData());
					flower.setGrowingTips(gt);
				}
				if(Constant.LIGHT.equals(currentElement)){
					flower.getGrowingTips().setLighting(characters.getData());
				}
				if(Constant.WATER.equals(currentElement)){
					flower.getGrowingTips().setWatering(characters.getData());
				}
				if(Constant.MULTI.equals(currentElement)){
					flower.setMultiplying(characters.getData());
				}

			}
			if(event.isEndElement()){
				EndElement endElement = event.asEndElement();
				String localName = endElement.getName().getLocalPart();

				if(Constant.FLOWER.equals(localName)){
					flowerList.getFlowers().add(flower);
					continue;
				}
			}
		}

		reader.close();


	}

}