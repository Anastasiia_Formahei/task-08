package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Constant;
import com.epam.rd.java.basic.task8.utils.Flower;
import com.epam.rd.java.basic.task8.utils.FlowerList;
import com.epam.rd.java.basic.task8.utils.GrowingTips;
import com.epam.rd.java.basic.task8.utils.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	private FlowerList flowerList;


	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public FlowerList getFlowerList(){
		return flowerList;
	}

	public void parse(boolean validate) throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);

			if(validate) {
				factory.setFeature("http://xml.org/sax/features/validation", true);
				factory.setFeature("http://apache.org/xml/features/validation/schema", true);
			}

				DocumentBuilder builder = factory.newDocumentBuilder();

			builder.setErrorHandler(new DefaultHandler(){
				@Override
				public void error(SAXParseException e) throws SAXException {
					throw e;
				}
			});
				Document document = builder.parse(xmlFileName);
				Element element = document.getDocumentElement();
				flowerList = new FlowerList();
				NodeList flowersList = element.getElementsByTagName(Constant.FLOWER);
				for (int i = 0; i < flowersList.getLength(); i++){
					Node node = flowersList.item(i);
					flowerList.getFlowers().add(getFlower(node));
				}
	}

	private Flower getFlower(Node node){
		Flower flower = new Flower();
		Element element = (Element) node;

		Node dataNode = element.getElementsByTagName(Constant.NAME).item(0);
		flower.setName(dataNode.getTextContent());

		dataNode = element.getElementsByTagName(Constant.SOIL).item(0);
		flower.setSoil(dataNode.getTextContent());

		dataNode = element.getElementsByTagName(Constant.ORIGIN).item(0);
		flower.setOrigin(dataNode.getTextContent());

		Node dataNodeVP = element.getElementsByTagName(Constant.VP).item(0);
		VisualParameters vp = getVisualParameters(dataNodeVP);
		flower.setVisualParameters(vp);

		Node dataNodeGT = element.getElementsByTagName(Constant.GT).item(0);
		GrowingTips growingTips = getGrowingTips(dataNodeGT);
		flower.setGrowingTips(growingTips);

		dataNode = element.getElementsByTagName(Constant.MULTI).item(0);
		flower.setMultiplying(dataNode.getTextContent());

		return flower;
	}
	private VisualParameters getVisualParameters(Node node){
		VisualParameters visualParameters = new VisualParameters();

		Element element = (Element) node;
		Node dataNode = element.getElementsByTagName(Constant.STEMCOLOUR).item(0);
		visualParameters.setStemColour(dataNode.getTextContent());

		dataNode = element.getElementsByTagName(Constant.LEAFCOLOUR).item(0);
		visualParameters.setLeafColour(dataNode.getTextContent());

		dataNode = element.getElementsByTagName(Constant.AVELENFLOWER).item(0);
		visualParameters.setAveLenFlower(dataNode.getTextContent());
		 return visualParameters;
	}

	private GrowingTips getGrowingTips(Node node){
		GrowingTips growingTips = new GrowingTips();

		Element element = (Element) node;
		Node dataNode = element.getElementsByTagName(Constant.TEMPRETURE).item(0);
		growingTips.setTempreture(dataNode.getTextContent());

		dataNode = element.getElementsByTagName(Constant.LIGHT).item(0);
		growingTips.setLighting(dataNode.getTextContent());

		dataNode = element.getElementsByTagName(Constant.WATER).item(0);
		growingTips.setWatering(dataNode.getTextContent());
		return growingTips;
	}


	public static Document getDocument(FlowerList flowerList) throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.newDocument();
		Element element = document.createElement("flowers");
		element.setAttribute("xmlns", "http://www.nure.ua");
		element.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		element.setAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd ");
		document.appendChild(element);

		for(Flower flower: flowerList.getFlowers()){

			Element flowerElement = document.createElement(Constant.FLOWER);

			Element data = document.createElement(Constant.NAME);
			data.setTextContent(flower.getName());
			flowerElement.appendChild(data);

			data = document.createElement(Constant.SOIL);
			data.setTextContent(flower.getSoil());
			flowerElement.appendChild(data);

			data = document.createElement(Constant.ORIGIN);
			data.setTextContent(flower.getOrigin());
			flowerElement.appendChild(data);

			VisualParameters vp = flower.getVisualParameters();

			data = document.createElement(Constant.VP);

			Element dataVP = document.createElement(Constant.STEMCOLOUR);
			dataVP.setTextContent(vp.getStemColour());
			data.appendChild(dataVP);

			dataVP = document.createElement(Constant.LEAFCOLOUR);
			dataVP.setTextContent(vp.getLeafColour());
			data.appendChild(dataVP);

			dataVP = document.createElement(Constant.AVELENFLOWER);
			dataVP.setAttribute("measure","cm");
			dataVP.setTextContent(vp.getAveLenFlower());
			data.appendChild(dataVP);
			flowerElement.appendChild(data);

			GrowingTips gt = flower.getGrowingTips();
			data = document.createElement(Constant.GT);

			Element dataGT = document.createElement(Constant.TEMPRETURE);
			dataGT.setAttribute("measure","celcius");
			dataGT.setTextContent(gt.getTempreture());
			data.appendChild(dataGT);

			dataGT = document.createElement(Constant.LIGHT);
			dataGT.setAttribute("lightRequiring","yes");
			dataGT.setTextContent(gt.getLighting());
			data.appendChild(dataGT);

			dataGT = document.createElement(Constant.WATER);
			dataGT.setAttribute("measure","mlPerWeek");
			dataGT.setTextContent(gt.getWatering());
			data.appendChild(dataGT);
			flowerElement.appendChild(data);

			data = document.createElement(Constant.MULTI);
			data.setTextContent(flower.getMultiplying());
			flowerElement.appendChild(data);

			element.appendChild(flowerElement);

		}

		return document;
	}

	public static void saveAsXmlDocument(Document document, String xmlFileName) throws TransformerException {
		StreamResult streamResult = new StreamResult(new File(xmlFileName));
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.transform(new DOMSource(document), streamResult);
	}

}
