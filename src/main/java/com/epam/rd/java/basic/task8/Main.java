package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.utils.FlowerList;


public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		FlowerList list = new FlowerList();
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);

		domController.parse(true);
		list = domController.getFlowerList();

		// save
		String outputXmlFile = "output.dom.xml";
		DOMController.saveAsXmlDocument(DOMController.getDocument(list),outputXmlFile );

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		saxController.parse(true);
		list = saxController.getFlowerList();
		
		// save
		outputXmlFile = "output.sax.xml";
		DOMController.saveAsXmlDocument(DOMController.getDocument(list),outputXmlFile );
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parse();
		list = staxController.getFlowerList();
		
		// save
		outputXmlFile = "output.stax.xml";
		DOMController.saveAsXmlDocument(DOMController.getDocument(list),outputXmlFile );

	}

}
