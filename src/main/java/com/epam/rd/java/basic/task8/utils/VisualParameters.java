package com.epam.rd.java.basic.task8.utils;

public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private String aveLenFlower;

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public String getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(String aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }
}
