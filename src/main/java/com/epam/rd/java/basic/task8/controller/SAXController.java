package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Constant;
import com.epam.rd.java.basic.task8.utils.Flower;
import com.epam.rd.java.basic.task8.utils.FlowerList;
import com.epam.rd.java.basic.task8.utils.GrowingTips;
import com.epam.rd.java.basic.task8.utils.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private FlowerList flowerList;
	private String currentElement;
	private Flower flower;


	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public FlowerList getFlowerList(){
		return flowerList;
	}
	public void parse(boolean validate) throws SAXException, ParserConfigurationException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);

		if(validate) {
			factory.setFeature("http://xml.org/sax/features/validation", true);
			factory.setFeature("http://apache.org/xml/features/validation/schema", true);
		}

		SAXParser parser = factory.newSAXParser();
		parser.parse(xmlFileName, this);
	}

	public void startElement(String uri, String localName, String qName, Attributes attributes){
		currentElement = localName;

		if(("flowers").equals(currentElement)){
			flowerList = new FlowerList();
			return;
		}
		if(Constant.FLOWER.equals(currentElement)){
			flower = new Flower();
		}
	}

	public void characters(char[] ch, int start, int length) {
		String element = new String(ch, start,length).trim();

		VisualParameters vp = new VisualParameters();
		GrowingTips gt = new GrowingTips();

		if(Constant.NAME.equals(currentElement)){
			flower.setName(element);
			return;
		}
		if(Constant.SOIL.equals(currentElement)){
			flower.setSoil(element);
			return;
		}
		if(Constant.ORIGIN.equals(currentElement)){
			flower.setOrigin(element);
			return;
		}
		if(Constant.STEMCOLOUR.equals(currentElement)){
			vp.setStemColour(element);
			flower.setVisualParameters(vp);
		}
		if(Constant.LEAFCOLOUR.equals(currentElement)){
			flower.getVisualParameters().setLeafColour(element);
		}
		if(Constant.AVELENFLOWER.equals(currentElement)){
			flower.getVisualParameters().setAveLenFlower(element);
		}
		if(Constant.TEMPRETURE.equals(currentElement)){
			gt.setTempreture(element);
			flower.setGrowingTips(gt);
		}
		if(Constant.LIGHT.equals(currentElement)){
			flower.getGrowingTips().setLighting(element);
		}
		if(Constant.WATER.equals(currentElement)){
			flower.getGrowingTips().setWatering(element);
		}
		if(Constant.MULTI.equals(currentElement)){
			flower.setMultiplying(element);
		}

	}

	public void endElement(String uri, String localName, String qName){
		if(Constant.FLOWER.equals(localName)){
			flowerList.getFlowers().add(flower);
		}
	}
}